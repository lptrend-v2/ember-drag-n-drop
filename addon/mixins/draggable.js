import Ember from 'ember';

export default Ember.Mixin.create({
  
  dragCoordinator: Ember.inject.service(),

  attributeBindings: ['draggable'],
  draggable: true,
  transferEffect: 'move',
  
  classNameBindings: ['isDragged:is-dragged'],
  isDragged: false,

// disable drag element by default, redefine to true for allow move this element
  allowDrag: false,

  x: Ember.computed({
    get() {
      if (this._x === undefined || isNaN(this._x)) {
        let marginLeft = parseFloat(this.$().css('margin-left'));
        this._x = this.element.scrollLeft + this.element.offsetLeft - marginLeft;
      }
      return this._x;
    },
    set(key, value) {
      if (value !== this._x/* && value > 0*/) {
        this._x = value;
        this._scheduleApplyPosition();
      }
    },
  }).volatile(),

  y: Ember.computed({
    get() {
      if (this._y === undefined || isNaN(this._y)) {
        this._y = this.element.offsetTop;
      }
      return this._y;
    },
    set(key, value) {
      if (value !== this._y/* && value > 0*/) {
        this._y = value;
        this._scheduleApplyPosition();
      }
    }
  }).volatile(),

  width: Ember.computed(function() {
    var el = this.$();
    return el.outerWidth(true);
  }).volatile(),

  height: Ember.computed(function() {
    var el = this.$();
    return el.outerHeight() + parseFloat(el.css('margin-bottom'));
  }).volatile(),
  
  inRect: function(x, y) { //FIXME: treshold & prevent jumping
    var right = this._x + this.get('width'),
        bottom = this._y + this.get('height');
    return (x >= this._x && x <= right) && (y >= this._y && y <= bottom);
  },
  
  isMustMove(position, isAhead) {
    var treshold = 30,
        right = this._x + this.get('width'),
        bottom = this._y + this.get('height'),
        center = {x: this._x + this.get('width')/2, y: this._y + this.get('height')/2};
    
    if (!(position.x >= this._x + treshold && position.x <= right - treshold) && 
        !(position.y >= this._y + treshold && position.y <= bottom - treshold)) {
      return false;
    }
    return (position.x >= this._x && position.x <= right) && (position.y >= this._y && position.y <= bottom);
  },
  
  didInsertElement: function() {
    var self = this;
    this._super();
    Ember.run.schedule('afterRender', this, function() {
      self.set('_parent', self.parentView);
      self._tellParent('addItem', self);
    });
  },

  willDestroyElement: function() {
//    Ember.run.schedule('afterRender', this, '_tellParent', 'removeItem', this);
    this._tellParent('removeItem', this);
    this._super();
  },
  
  dragStart: function(event) {
    event.stopPropagation();

    if (!this.allowDrag) { // prevent dragging & bubbling event
      event.preventDefault();
      return false;      
    }
    
    this._dragStart(event);
  },
  
  _dragStart: function(event) {
    this.get('dragCoordinator').dragStarted(this);
    
    var dragIcon = document.createElement('img');

    event.dataTransfer.setData('Object', this);
    dragIcon.src = 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7';
    dragIcon.width = 1;
    dragIcon.height = 1;
    event.dataTransfer.setDragImage(dragIcon, 0, 0);
    event.dataTransfer.effectAllowed = this.get('transferEffect');

    this.set('isDragged', true);
    Ember.Logger.info('--- start dragging in', this.get('elementId')/*,this.get('origin')*/);
//    this._tellParent('_dragStarted');
  },

  dragEnd: function(/*event*/) {
    this.get('dragCoordinator').dragEnded(this);
    this._tellParent('complete');
    this.set('isDragged', false);
    return false;      
  },
  
  _drag: function(event) {
    event.preventDefault();
    event.stopPropagation();

    var parentRect = this._parent.element.getBoundingClientRect(),
        { x, y } = getEventCoordinates(event);

    this.setProperties({'x': x-parentRect.left, 'y': y-parentRect.top});
    Ember.run.throttle(this, '_tellParent', 'update', 150);
    return false;
  },

  _tellParent: function(method, ...args) {
    let parent = this._parent;
    if (Ember.typeOf(parent) !== 'instance') {
      Ember.assert('[Draggable mixin] The `parent` are empty in',this);
      return false;
    }
    if (method in parent) {
      parent[method](...args);
    } else {
      Ember.Logger.warn('[Draggable] parent hasn`t method',method,parent);
    }
  },

  _reset: function() {
    var elem = this.$();

    delete this._y;
    delete this._x;

    elem.css({ transform: '' });
    elem.height(); // force apply styles
  },
  
  _scheduleApplyPosition() {
    Ember.run.scheduleOnce('render', this, '_applyPosition');
  },
  
  _applyPosition: function() {
    var elem = this.element,
        dx = this.get('x') - elem.offsetLeft + parseFloat(this.$().css('margin-left')),
        dy = this.get('y') - elem.offsetTop,
        transform = `translateX(${dx}px) translateY(${dy}px)`;

    elem.style.transform = transform;
    this.$().css({ transform: transform });
  }
  
});

/**
  Gets the touch or mouse coordinates for a given event.
  @method getEventCoordinates
  @return {Object} {x:Number, y:Number}
  @private
*/
function getEventCoordinates(event) {
  var originalEvent = event.originalEvent,
      touches = originalEvent && originalEvent.changedTouches,
      touch = touches && touches[0];

  if (touch) {
    return { x: touch.screenX, y: touch.screenY };
  }
  
  return { x: originalEvent.pageX, y: originalEvent.pageY };
}

/*  mouseDown(event) {
    //TODO: modificator key: Alt - copy ???
    this.startDragging(event);
  },
  touchStart(event) {
    this.startDragging(event);
  },
*/

