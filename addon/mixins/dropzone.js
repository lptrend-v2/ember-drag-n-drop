﻿import Ember from 'ember';
import generateUUID from 'lptrendv2/mixins/generate-uuid';

export default Ember.Mixin.create(generateUUID, {
  
  store: Ember.inject.service(),
  dragCoordinator: Ember.inject.service(),
  
  allowDrop: true,
  allow: [],
  disallow: [],
  
  items: null,
  prevDragged: null,
  
  init() {
    this._super(...arguments);
    this.set('items', Ember.A());
  },
  
  isAllow: function(types) {
    if (!this.get('allowDrop')) {
      return false;
    }
    
    var self = this, 
        _allow = (self.allow.length > 0),
        _disallow = (self.disallow.length > 0),
        allow = (!_allow && _disallow) || (!_allow);
    
    [].forEach.call(types, function(type) { // firefox pleased...
      if (type !== 'text/json') {  
        if (allow && _disallow && self.disallow.indexOf(type) !== -1) {
          allow = false;
        } else if (!allow && _allow && self.allow.indexOf(type) !== -1){
          allow = true;
        }
      }
    }); 
    return allow;
  },
  
  sortedItems: Ember.computed(function() {
    var items = Ember.ArrayProxy.create({ content: this.get('items') }),
        dragged = this.get('items').findBy('isDragged'),
        dragIndex = items.indexOf(dragged),
        position = {
          x: dragged.get('x'),
          y: dragged.get('y'),
          width: dragged.get('width'),
          height: dragged.get('height')
        };
    
    var matchedItem = items.find(function(item, index) {
      if (item === dragged) {
        return false;
      }
//      return item.inRect(position.x, position.y);
      return item.isMustMove(position, index < dragIndex);
    });
    
    if (matchedItem !== undefined) {
      var matchedIndex = items.indexOf(matchedItem);
      items = items.without(dragged);
      items.insertAt(matchedIndex, dragged);
    }
    
    return items;
  }).volatile(),
  
  dragOver: function(event) {
    if (!this.isAllow(event.dataTransfer.types)) { //bubble event
      return true;
    }
    
    this.get('dragCoordinator').draggingOver(event, this);
    return false;    
  },
  
  dragEnter: function(event) {
    if (!this.isAllow(event.dataTransfer.types)) { //bubble event
      return true;
    }
    
    if (event.dataTransfer.effectAllowed === 'move') { //stop bubbling, handle it in dragOver()
      return false;
    }
    Ember.Logger.info('--- [Dropzone] '+this+' enter attempt...');
    
    var dragCoordinator = this.get('dragCoordinator'),
        dragObject = dragCoordinator.getDragged();
    
    if (dragObject === this || this.get('prevDragged') !== null) {
      return false; //yo-ho! catch self dragging
    }
    this.parentView._dragLeave(event);
    dragObject = dragCoordinator.getDragged(); //refresh dragObject after releasing by parent
    Ember.assert('[Dropzone]'+this+' dragObject is instance '+dragObject, Ember.typeOf(dragObject) !== 'instance');
    Ember.Logger.info('[Dropzone]'+this+' enter — '+Ember.typeOf(dragObject), dragObject);
    
    this.set('prevDragged', dragObject);
    dragCoordinator.storeEvent(event);
    
    this._appendChild(dragObject);    
    return false;
  },
  
  dragLeave: function(event) {
    if (event.dataTransfer.effectAllowed === 'move') {
      return false;
    }
    if (this.get('prevDragged') !== null) {
      var elRect = this.element.getBoundingClientRect(),
          cX = event.originalEvent.clientX,
          cY = event.originalEvent.clientY,
          outsideElement = (cX < elRect.left || cX > elRect.right || cY < elRect.top || cY > elRect.bottom) || false;

      if (outsideElement) {
        this._dragLeave(event);
      }
      event.preventDefault();
      return false;
    }
//    return false;
  },
  
  _dragLeave(/*event*/) {
    
    if (this.get('prevDragged') === null) {
      Ember.Logger.warn('[Dropzone]'+this+' prevDragged is null');
      return false;
    }
    
    var dragCoordinator = this.get('dragCoordinator'),
        dragObject = dragCoordinator.getDragged();
    Ember.Logger.info('[Dropzone]'+this+' leave — '+dragObject);
    this.get('content.childs').removeObject(dragObject);
    if (Ember.typeOf(dragObject) === 'instance') {
      Ember.Logger.info('--- clean up '+dragObject);
//        dragObject.get('content').destroyRecord();
      dragObject.get('content').deepUnload(['parent']);
    } else {
      Ember.Logger.info('--- skip clean up '+dragObject, Ember.typeOf(dragObject));
    }
    dragCoordinator.dragStarted(this.get('prevDragged'));
    this.set('prevDragged', null);
    Ember.run.schedule('afterRender', this, 'update');
  },
  
  drop: function(event) {
    Ember.Logger.log('Drop in '+this.get('content.type'), this);
    var data = null;
    if (event.dataTransfer.effectAllowed === 'move') {
      return true;
    }
    
    if (this.isAllow(event.dataTransfer.types)) {
      if (this.get('prevDragged') !== null) {
        var dragCoordinator = this.get('dragCoordinator'),
            dragObject = dragCoordinator.getDragged();
        dragObject.dragEnd(event);
        this.set('prevDragged', null);
      } else {
        [].forEach.call(event.dataTransfer.types, function(type) { // firefox pleased...
          Ember.Logger.log('('+type+')', event.dataTransfer.getData(type));
          if (type === 'text/json') {
            data = JSON.parse(event.dataTransfer.getData(type));
          }
        });
        Ember.assert('[Dropzone] empty data in dropped item'+event, data !== null);
        this._appendChild(data);
      }

//NOTE : allow return false or event.stopPropagation() ?
      event.preventDefault();
      event.stopPropagation();
      return false;
    }
    Ember.Logger.info('--- allowDrop', this.get('allowDrop'), 'isAllow', this.isAllow(event.dataTransfer.types));
    Ember.Logger.info('--- types '+event.dataTransfer.types, 'allow '+this.allow, '| disallow '+this.disallow);
    return true;
  },
  
//  _dragStarted: function() {
//  },

  addItem: function(item) {
    this.get('items').addObject(item);
    if (item.get('content.isNew') && !item.get('content.parent.isNew')) {
      var dragCoordinator = this.get('dragCoordinator');
      Ember.Logger.info('------ addItem new item'+item,'#',item.get('content.id'));
      item._dragStart(dragCoordinator.getEvent());
    }
  },
  
  removeItem: function(item) {
    this.get('items').removeObject(item);
  },
  
  update: function() {
    var items = this.get('sortedItems'),
        width = this.$().width(),
        position = { 
          x: (this.$().innerWidth()-width)/2 ,
          y: (this.$().innerHeight()-this.$().height())/2
        },
        _x = position.x,
        _y = 0;
    
    items.forEach(function(item) {
      if (position.x + item.get('width') > width + _x) {
        position.x = _x;
        position.y += _y;
      }
      
      if (!item.get('isDragged')) {
        item.setProperties({'x': position.x, 'y': position.y });
      }
      
      position.x += item.get('width');
      _y = item.get('height');
    });
    this.set('items', items);
  },
  
  complete: function() {
    var items = this.get('sortedItems'),
        itemModels = items.mapBy('content');
    
    this.get('content.childs').then(function(childs) {
      items.invoke('_reset');
      Ember.run.scheduleOnce('afterRender', function() {
        childs.setObjects(itemModels);
      });
    });
  },
  
  _appendChild: function(element) {
    var self = this,
        content = this.get('content'),
        childs = this.get('content.childs'),
        store = this.get('store');
    
    Ember.assert('[Dropzone] can`t peek childs in '+this, Ember.typeOf(childs) === 'instance');

    Ember.Logger.info('[Dropzone]'+this+' add new child '+element.type);
    
    if (element.hasOwnProperty('duplicate') && element.duplicate === true) {
      var sample = store.peekRecord(element.type, element.id);
      
      Ember.assert('[Dropzone] can`t peek element', element !== null);
      
      sample.duplicate({ parent: content }).then(function (double) {
        childs.addObject(double);
        Ember.Logger.info('[Dropzone]'+self+' child ID',double.id);
      });
    } else {
      var payload = {};

      for (var key in element) {
        switch (key) {
          case 'id':
          case 'type':
          case 'icon':
            break;

          default:
            payload[key] = element[key];
            break;
        }
      }
      payload.id = this.generateUUID();
      payload.parent = content;
      element = store.createRecord(element.type, payload);
      childs.addObject(element);    
      Ember.Logger.info('[Dropzone]'+this+' child ID',element.id);
    }
  }
});
